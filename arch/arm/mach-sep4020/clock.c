/* linux/arch/arm/mach-sep4020/clock.c
 *
 *changelog
*2009	first version basic function
*2010 0203 fixed the clk->usage, there are only two status here, be sure enable(or disable) clk is success
*
*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/errno.h>
#include <linux/err.h>
#include <linux/platform_device.h>
#include <linux/sysdev.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <linux/clk.h>
#include <linux/mutex.h>

#include <asm/hardware.h>
#include <asm/irq.h>
#include <asm/io.h>

#include "clock.h"

/* clock information */

static LIST_HEAD(clocks);
static DEFINE_MUTEX(clocks_mutex);

/* inline functions */

void inline sep4020_clk_enable(unsigned int ctrlbit, unsigned int enable)
{
	unsigned long clkcon;
	clkcon = *(volatile unsigned long*)PMU_PCSR_V;

	if (enable)
		clkcon |= ctrlbit;
	else
		clkcon &= ~ctrlbit;

	/* ensure none of the special function bits clear */
	clkcon |= (SEP4020_PCSR_TIMER|SEP4020_PCSR_DMAC|SEP4020_PCSR_EMI|SEP4020_PCSR_ESRAM);
	*(volatile unsigned long*)PMU_PCSR_V = clkcon;
}

/* enable and disable calls for use with the clk struct */

static int clk_null_enable(struct clk *clk, int enable)
{
	return 0;
}

int sep4020_clkcon_enable(struct clk *clk, int enable)
{
	sep4020_clk_enable(clk->ctrlbit, enable);
	return 0;
}

/* Clock API calls */

struct clk *clk_get(struct device *dev, const char *id)
{
	struct clk *p;
	struct clk *clk = ERR_PTR(-ENOENT);

	mutex_lock(&clocks_mutex);
/* i suppose all the drivers are based on plartform device architecture */
	list_for_each_entry(p, &clocks, list) {
		if (strcmp(id, p->name) == 0 &&
		    try_module_get(p->owner)) {
			clk = p;
			break;
		}
	}

	mutex_unlock(&clocks_mutex);
	return clk;
}

void clk_put(struct clk *clk)
{
	module_put(clk->owner);
}

int clk_enable(struct clk *clk)
{
	if (IS_ERR(clk) || clk == NULL)
		return -EINVAL;
	
	printk("clk_enable*****************************, the module is %s\n", clk->name);
	mutex_lock(&clocks_mutex);
	if ((clk->usage) == 0)
	{
		(clk->enable)(clk, 1);
		clk->usage++;
	}	
	mutex_unlock(&clocks_mutex);
	return 0;
}

void clk_disable(struct clk *clk)
{
	if (IS_ERR(clk) || clk == NULL)
		return;

	printk("clk_disable****************************, the module is %s\n", clk->name);
	mutex_lock(&clocks_mutex);

	if ((clk->usage-1) == 0)
	{
		(clk->enable)(clk, 0);
		clk->usage--;	
	}
	mutex_unlock(&clocks_mutex);
}

/**we just return the main clock, because there is no difference between AMBA and the core
we suppose the main freq will not below 32MHZ, the units is KHz in here**/
unsigned long clk_get_rate(struct clk *clk)
{
	int pmcr_pre;

	pmcr_pre = *(volatile unsigned long*)PMU_PMCR_V ;
	if(pmcr_pre > 0x4000)
		return (pmcr_pre-0x4000)*8000;
	else 
		return (pmcr_pre)*4000;
}

long clk_round_rate(struct clk *clk, unsigned long rate)
{
	return rate;
}

int clk_set_rate(struct clk *clk, unsigned long rate)
{
	return -EINVAL;
}



EXPORT_SYMBOL(clk_get);
EXPORT_SYMBOL(clk_put);
EXPORT_SYMBOL(clk_enable);
EXPORT_SYMBOL(clk_disable);
EXPORT_SYMBOL(clk_get_rate);
EXPORT_SYMBOL(clk_round_rate);
EXPORT_SYMBOL(clk_set_rate);


/* clock definitions */

static struct clk init_clocks[] = {
	{
		.name		= "esram",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_ESRAM,
	}, 
{
		.name		= "lcdc",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_LCDC,
	}, 
{
		.name		= "pwm",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_PWM,
	}, 
{
		.name		= "dmac",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_DMAC,
	}, 
{
		.name		= "emi",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_EMI,
	}, 
{
		.name		= "sdi",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_MMCSD,
	}, 
{
		.name		= "ssi",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_SSI,
	}, 
{
		.name		= "uart0",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_UART0,
	}, 
{
		.name		= "uart1",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_UART1,
	}, 
{
		.name		= "uart2",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_UART2,
	}, 
{
		.name		= "uart3",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_UART3,
	}, 
{
		.name		= "usb",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_USB,
	}, 
{
		.name		= "ether_clk",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_MAC,
	}, 
{
		.name		= "smc",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_SMC,
	}, 
{
		.name		= "i2s",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_I2S,
	}, 
{
		.name		= "timer",
		.enable		= sep4020_clkcon_enable,
		.ctrlbit	= SEP4020_PCSR_TIMER,
	} 
};

/* initialise the clock system */

int sep4020_register_clock(struct clk *clk)
{
	clk->owner = THIS_MODULE;

	if (clk->enable == NULL)
		clk->enable = clk_null_enable;

	/* if this is a standard clock, set the usage state */

	if (clk->ctrlbit) {
		unsigned long clkcon = *(volatile unsigned long*)PMU_PCSR_V;

		clk->usage = (clkcon & clk->ctrlbit) ? 1 : 0;
	}

	/* add to the list of available clocks */

	mutex_lock(&clocks_mutex);
	list_add(&clk->list, &clocks);
	mutex_unlock(&clocks_mutex);

	return 0;
}

/* initalise all the clocks */

int __init sep4020_setup_clocks(void)
{
	struct clk *clkp = init_clocks;
	int ptr;
	int ret;
	int sep4020_main_clock;

//	printk("SEP4020 Clocks, 2009 prochip\n");

	/* initialise the main system clocks */
	/*here need to fix the uart */


	mutex_lock(&clocks_mutex);
	
	/*disable follow module's clock*/


	sep4020_clk_enable(SEP4020_PCSR_LCDC, 0);
	sep4020_clk_enable(SEP4020_PCSR_PWM, 0);
	sep4020_clk_enable(SEP4020_PCSR_MMCSD, 0);
	sep4020_clk_enable(SEP4020_PCSR_USB, 0);
	sep4020_clk_enable(SEP4020_PCSR_MAC, 0);
	sep4020_clk_enable(SEP4020_PCSR_SMC, 0);
	sep4020_clk_enable(SEP4020_PCSR_I2S, 0);


	mutex_unlock(&clocks_mutex);
 

	/* assume uart clocks are correctly setup */

	/* register clocks from clock array */

	for (ptr = 0; ptr < ARRAY_SIZE(init_clocks); ptr++, clkp++) {
		ret = sep4020_register_clock(clkp);
		if (ret < 0) {
//			printk(KERN_ERR "Failed to register clock %s (%d)\n", clkp->name, ret);
		}
	}

	/* show the clock-slow value */
	sep4020_main_clock = clk_get_rate(NULL);

//	printk("SEP4020 CLOCK:%dMHz\n", sep4020_main_clock/1000);

	return 0;
}

arch_initcall(sep4020_setup_clocks);
