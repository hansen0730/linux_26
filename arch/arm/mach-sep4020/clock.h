/*
 * linux/arch/arm/mach-sep4020/clock.h
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

struct clk {
	struct list_head      list;
	struct module        *owner;
	struct clk           *parent;
	const char           *name;
	int		      usage;
	unsigned long         rate;
	unsigned long         ctrlbit;
	int		    (*enable)(struct clk *, int enable);
};

/* other clocks which may be registered by board support */
/*
extern struct clk s3c24xx_dclk0;
extern struct clk s3c24xx_dclk1;
extern struct clk s3c24xx_clkout0;
extern struct clk s3c24xx_clkout1;
extern struct clk s3c24xx_uclk;
*/
/* exports for arch/arm/mach-s3c2410
 *
 * Please DO NOT use these outside of arch/arm/mach-sep4020
*/

#define	SEP4020_PCSR_ESRAM	1<<0
#define	SEP4020_PCSR_LCDC		1<<1
#define	SEP4020_PCSR_PWM		1<<2
#define	SEP4020_PCSR_DMAC		1<<3
#define	SEP4020_PCSR_EMI		1<<4
#define	SEP4020_PCSR_MMCSD	1<<5
#define	SEP4020_PCSR_RESERVED	1<<6
#define	SEP4020_PCSR_SSI		1<<7
#define	SEP4020_PCSR_UART0	1<<8
#define	SEP4020_PCSR_UART1	1<<9
#define	SEP4020_PCSR_UART2	1<<10
#define	SEP4020_PCSR_UART3	1<<11
#define	SEP4020_PCSR_USB		1<<12
#define	SEP4020_PCSR_MAC		1<<13
#define	SEP4020_PCSR_SMC		1<<14
#define	SEP4020_PCSR_I2S		1<<15
#define	SEP4020_PCSR_TIMER	1<<16


//extern int sep4020_clkcon_enable(struct clk *clk, int enable);
//extern int sep4020_register_clock(struct clk *clk);
//extern int sep4020_setup_clocks(unsigned long xtal, unsigned long fclk, unsigned long hclk, unsigned long pclk);

