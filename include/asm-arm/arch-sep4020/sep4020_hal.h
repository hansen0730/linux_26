/*
 *  linux/include/asm-arm/arch-sep4020/sep4020_hal.h
 *
 *  This file contains the hardware definitions of the Prospector P720T.
 *
 *  Copyright (C) 2000 Deep Blue Solutions Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 /*
 *该文件为sep4020各类开发板的硬件抽象层，这个文件只针对与PCB版相关的GPIO口进行处理，一般寄存器不进行处理，如果硬件上有什么差异，请大家修改这个文件
 */
#ifndef __ASM_ARCH_HAL_H
#define __ASM_ARCH_HAL_H
   
#include "hardware.h"
#include "irqs.h"

#define REG32(addr)	(*(volatile unsigned int *)(addr))
/*-----------port or many bit operation------------------------*/
#define SET_BIT(port,bit) REG32(port) |= (1 << bit)
#define CLR_BIT(port,bit) REG32(port) &= (~(1 << bit))
#define GET_BIT(port,bit) (REG32(port) & (1 << bit))
 
#define SET_PORT_VALUE(port,value,mask) ({unsigned long res_port = REG32(port); REG32(port) = (res_port & (~mask)) | (value & mask); })
#define SET_PORT_MASK(port,mask) REG32(port) |= mask
#define CLR_PORT_MASK(port,mask) REG32(port) &= (~mask)
#define GET_PORT_MASK(port,mask) (REG32(port) & mask)
/*--------------------------EXTERN INTERRUPT----------------------------------------*/
/*---------中断类型定义------------------------------------------------------*/
#define RISING_EDGE_TRIG      0x0
#define FALLING_EDGE_TRIG     0x1
#define HIGHT_LEVEL_TRIG      0x2
#define LOW_LEVEL_TRIG        0x3

#define CONFIG_INT(int_num,int_type)   \
do{    \
	if(int_num == 11){  \
		(*(volatile unsigned long*)GPIO_PORTF_SEL_V) |= 0x0001 ;(*(volatile unsigned long*)GPIO_PORTF_DIR_V) |= 0x0001 ; (*(volatile unsigned long*)GPIO_PORTF_INTRCTL_V) |= int_type;(*(volatile unsigned long*)GPIO_PORTF_INCTL_V) |= 0x0001;(*(volatile unsigned long*)GPIO_PORTF_INTRCLR_V) |= 0x0001;(*(volatile unsigned long*)GPIO_PORTF_INTRCLR_V) = 0x0000;}\
	else if(int_num < 11){      \
		*(volatile unsigned long*)GPIO_PORTA_SEL_V |= 0x1 << (int_num - 1);*(volatile unsigned long*)GPIO_PORTA_DIR_V |= 0x1 << (int_num - 1);*(volatile unsigned long*)GPIO_PORTA_INTRCTL_V |= int_type << ((int_num - 1)*2);*(volatile unsigned long*)GPIO_PORTA_INCTL_V |= 0x1 << (int_num - 1);*(volatile unsigned long*)GPIO_PORTA_INTRCLR_V |= 0x1 << (int_num - 1);*(volatile unsigned long*)GPIO_PORTA_INTRCLR_V = 0x0000;}\
}while(0)
#define CLR_INT(int_num)  \
do{   \
	if(int_num == 11){  \
		*(volatile unsigned long*)GPIO_PORTF_INTRCLR_V |= 0x0001;*(volatile unsigned long*)GPIO_PORTF_INTRCLR_V = 0x0000;}  \
	else if(int_num < 11){   \
		*(volatile unsigned long*)GPIO_PORTA_INTRCLR_V |= 0x1 << (int_num - 1);*(volatile unsigned long*)GPIO_PORTA_INTRCLR_V = 0x0000;}  \
}while(0)


/* --------------------------------------------------------------------
 * key board interface
 键盘原理图示意图，具体请参考各类板的原理图
                                     
          Col5(PA4)      Col4(PA3)   Col3(PA2)   Col2(PA1)  Col1(PA0)
                                      |                                  |                                |                               |                               |
Row5(PD4)-----|------------|-----------|-----------|-----------|---
                                      |                                  |                                |                               |                               |
Row4(PD3) ----|------------|-----------|-----------|-----------|---
                                      |                                  |                                |                               |                               |
Row3(PD2)-----|------------|-----------|-----------|-----------|---
                                      |                                  |                                |                               |                               |
Row2(PD1) ----|------------|-----------|-----------|-----------|---
                                      |                                  |                                |                               |                               |
Row1(PD0) ----|------------|-----------|-----------|-----------|---
* -------------------------------------------------------------------- */
 /*---key interrupt define----------*/
#define INT_SEL_PORT INTSRC_EXTINT0

#define COL_INT_TYPE LOW_LEVEL_TRIG  //定义键盘的中断触发类型是低电平触发
 
 /*---------key port define and gpio define-----------------------------*/
#define COL_MASK (0x1)
#define COL_SEL_PORT GPIO_PORTA_SEL_V
#define COL_DIR_PORT GPIO_PORTA_DIR_V
#define COL_DATA_PORT GPIO_PORTA_DATA_V 


#define COL1_BIT 0
#define COL1_SEL_PORT GPIO_PORTA_SEL_V
#define COL1_DIR_PORT GPIO_PORTA_DIR_V
#define COL1_DATA_PORT GPIO_PORTA_DATA_V 

#define COL2_BIT 1
#define COL2_SEL_PORT GPIO_PORTA_SEL_V
#define COL2_DIR_PORT GPIO_PORTA_DIR_V
#define COL2_DATA_PORT GPIO_PORTA_DATA_V 

#define COL3_BIT 2
#define COL3_SEL_PORT GPIO_PORTA_SEL_V
#define COL3_DIR_PORT GPIO_PORTA_DIR_V
#define COL3_DATA_PORT GPIO_PORTA_DATA_V 

#define COL4_BIT 3
#define COL4_SEL_PORT GPIO_PORTA_SEL_V
#define COL4_DIR_PORT GPIO_PORTA_DIR_V
#define COL4_DATA_PORT GPIO_PORTA_DATA_V 

#define COL5_BIT 4
#define COL5_SEL_PORT GPIO_PORTA_SEL_V
#define COL5_DIR_PORT GPIO_PORTA_DIR_V
#define COL5_DATA_PORT GPIO_PORTA_DATA_V 

#define ROW_MASK ( 0x1f)
#define ROW_SEL_PORT GPIO_PORTG_SEL_V 
#define ROW_DIR_PORT GPIO_PORTG_DIR_V
#define ROW_DATA_PORT GPIO_PORTG_DATA_V  
 /* -------------------------------------------------------------------- */
 /*---key interrupt define----------*/
/*#define COL1_INT INTSRC_EXTINT0
#define COL2_INT INTSRC_EXTINT1
#define COL3_INT INTSRC_EXTINT2
#define COL4_INT INTSRC_EXTINT3
#define COL5_INT INTSRC_EXTINT4

#define COL_INT_TYPE LOW_LEVEL_TRIG  *///定义键盘的中断触发类型是低电平触发
 
 /*---------key port define and gpio define-----------------------------*/
/*#define COL_MASK (0x1f)
#define COL_SEL_PORT GPIO_PORTA_SEL_V
#define COL_DIR_PORT GPIO_PORTA_DIR_V
#define COL_DATA_PORT GPIO_PORTA_DATA_V 


#define COL1_BIT 0
#define COL1_SEL_PORT GPIO_PORTA_SEL_V
#define COL1_DIR_PORT GPIO_PORTA_DIR_V
#define COL1_DATA_PORT GPIO_PORTA_DATA_V 

#define COL2_BIT 1
#define COL2_SEL_PORT GPIO_PORTA_SEL_V
#define COL2_DIR_PORT GPIO_PORTA_DIR_V
#define COL2_DATA_PORT GPIO_PORTA_DATA_V 

#define COL3_BIT 2
#define COL3_SEL_PORT GPIO_PORTA_SEL_V
#define COL3_DIR_PORT GPIO_PORTA_DIR_V
#define COL3_DATA_PORT GPIO_PORTA_DATA_V 

#define COL4_BIT 3
#define COL4_SEL_PORT GPIO_PORTA_SEL_V
#define COL4_DIR_PORT GPIO_PORTA_DIR_V
#define COL4_DATA_PORT GPIO_PORTA_DATA_V 

#define COL5_BIT 4
#define COL5_SEL_PORT GPIO_PORTA_SEL_V
#define COL5_DIR_PORT GPIO_PORTA_DIR_V
#define COL5_DATA_PORT GPIO_PORTA_DATA_V 

#define ROW_MASK ( 0x1f)
#define ROW_SEL_PORT GPIO_PORTD_SEL_V 
#define ROW_DIR_PORT GPIO_PORTD_DIR_V
#define ROW_DATA_PORT GPIO_PORTD_DATA_V  

#define ROW1_BIT 0
#define ROW1_SEL_PORT GPIO_PORTD_SEL_V 
#define ROW1_DIR_PORT GPIO_PORTD_DIR_V
#define ROW1_DATA_PORT GPIO_PORTD_DATA_V  

#define ROW2_BIT 1
#define ROW2_SEL_PORT GPIO_PORTD_SEL_V 
#define ROW2_DIR_PORT GPIO_PORTD_DIR_V
#define ROW2_DATA_PORT GPIO_PORTD_DATA_V  

#define ROW3_BIT 2
#define ROW3_SEL_PORT GPIO_PORTD_SEL_V 
#define ROW3_DIR_PORT GPIO_PORTD_DIR_V
#define ROW3_DATA_PORT GPIO_PORTD_DATA_V 

#define ROW4_BIT 3
#define ROW4_SEL_PORT GPIO_PORTD_SEL_V 
#define ROW4_DIR_PORT GPIO_PORTD_DIR_V
#define ROW4_DATA_PORT GPIO_PORTD_DATA_V  

#define ROW5_BIT 4
#define ROW5_SEL_PORT GPIO_PORTD_SEL_V 
#define ROW5_DIR_PORT GPIO_PORTD_DIR_V
#define ROW5_DATA_PORT GPIO_PORTD_DATA_V */

/*******************************************************************/
 /*sep4020 ether mac*/
/*******************************************************************/
#define PHY_INT INTSRC_EXTINT7
#define PHY_INT_TYPE LOW_LEVEL_TRIG  //定义键盘的中断触发类型是低电平触发

/*******************************************************************/
 /*nand flash*/
/*******************************************************************/
#define NAND_WP_BIT 7
#define NAND_WP_SEL_PORT GPIO_PORTG_SEL_V
#define NAND_WP_DIR_PORT GPIO_PORTG_DIR_V
#define NAND_WP_DATA_PORT GPIO_PORTG_DATA_V 

/********************************************************************/
/*led test*/
/********************************************************************/
#define LED_TEST_SEL_PORT GPIO_PORTE_SEL_V
#define LED_TEST_DIR_PORT GPIO_PORTE_DIR_V
#define LED_TEST_DATA_PORT GPIO_PORTE_DATA_V

/********************************************************************/
/*PSAM*/
/********************************************************************/
/*--------------------PSAM_CARD POS板----------------------------------------*/
#define PSAM_RDWR_DIR(dir)	\
do{	\
	 if(!dir){	\
		*(volatile unsigned long*)GPIO_PORTG_SEL_V |= 0x01;*(volatile unsigned long*)GPIO_PORTG_DIR_V &= (~0x01);*(volatile unsigned long*)GPIO_PORTG_DATA_V &= (~0x01);}\
	else if(dir){	\
		*(volatile unsigned long*)GPIO_PORTG_SEL_V |= 0x01;*(volatile unsigned long*)GPIO_PORTG_DIR_V &= (~0x01);*(volatile unsigned long*)GPIO_PORTG_DATA_V |= 0x01;}\
}while(0)

#define PSAM_CONFIG_REG		GPIO_PORTE_SEL_V
#define PSAM_RW_PORT    GPIO_PORTE_DATA_V
/*--------------------*外扩PSAM  POS版宏定义-----------------------*/
#define  PSAM_INT  INTSRC_EXTINT9
#define  PSAM_SEL_PORT	GPIO_PORTE_SEL_V
#define  PSAM_DIR_PORT	GPIO_PORTE_DIR_V
/********************************************************************/
/* uda1341 -IIS*/
/********************************************************************/
 

#define L3_SEL_PORT GPIO_PORTD_SEL_V
#define L3_DIR_PORT GPIO_PORTD_DIR_V
#define L3_DATA_PORT GPIO_PORTD_DATA_V

#define L3C_BIT 4              //GPD4 = L3CLOCK	//add by kyon
#define L3M_BIT 3              //GPD3 = L3MODE
#define L3D_BIT 1              //GPD1 = L3DATA

#define IIS_SEL_PORT GPIO_PORTG_SEL_V
#define IIS_DIR_PORT GPIO_PORTG_DIR_V
#define IIS_DATA_PORT GPIO_PORTG_DATA_V
#define IIS_BIT 11  

/***********************************************************************/
 /*hdlc port define and gpio define*/
/**********************************************************************/
#define HDLC_BIT 13 
#define HDLC_SEL_PORT GPIO_PORTG_SEL_V
#define HDLC_DIR_PORT GPIO_PORTG_DIR_V
#define HDLC_DATA_PORT GPIO_PORTG_DATA_V  

/*******************************************************************************/
/*touch panel*/
/*******************************************************************************/
/*-------------------touch panel (tp) interrupt define--------------*/
#define TP_INT     INTSRC_EXTINT8
            
      
/*---------------tp  port define and gpio define---------------------*/
#define TP1_BIT 3
#define TP1_SEL_PORT GPIO_PORTD_SEL_V 
#define TP1_DIR_PORT GPIO_PORTD_DIR_V
#define TP1_DATA_PORT GPIO_PORTD_DATA_V 

#define TP2_BIT 4
#define TP2_SEL_PORT GPIO_PORTD_SEL_V 
#define TP2_DIR_PORT GPIO_PORTD_DIR_V
#define TP2_DATA_PORT GPIO_PORTD_DATA_V 

#define TP3_BIT 1
#define TP3_SEL_PORT GPIO_PORTD_SEL_V 
#define TP3_DIR_PORT GPIO_PORTD_DIR_V
#define TP3_DATA_PORT GPIO_PORTD_DATA_V 

#define TP4_BIT  0x0
#define TP4_SEL_PORT GPIO_PORTD_SEL_V 
#define TP4_DIR_PORT GPIO_PORTD_DIR_V
#define TP4_DATA_PORT GPIO_PORTD_DATA_V 

#define TP5_MASK (0x100)
#define TP5_SEL_PORT GPIO_PORTA_SEL_V 
#define TP5_DIR_PORT GPIO_PORTA_DIR_V
#define TP5_DATA_PORT GPIO_PORTA_DATA_V 

#define TP6_MASK (0x01)
#define TP6_SEL_PORT GPIO_PORTD_SEL_V 
#define TP6_DIR_PORT GPIO_PORTD_DIR_V
#define TP6_DATA_PORT GPIO_PORTD_DATA_V 

#define TP7_MASK (0x1a)
#define TP7_SEL_PORT GPIO_PORTD_SEL_V 
#define TP7_DIR_PORT GPIO_PORTD_DIR_V
#define TP7_DATA_PORT GPIO_PORTD_DATA_V 

#define TP8_MASK (0x1b)
#define TP8_SEL_PORT GPIO_PORTD_SEL_V 
#define TP8_DIR_PORT GPIO_PORTD_DIR_V
#define TP8_DATA_PORT GPIO_PORTD_DATA_V 

#define TP9_MASK 0xFFFFFFFF
#define TP9_SEL_PORT GPIO_PORTA_SEL_V 
#define TP9_DIR_PORT GPIO_PORTA_DIR_V
#define TP9_DATA_PORT GPIO_PORTA_DATA_V 

/********************************************************************************/
/*Magnetic strip card port define*/
/********************************************************************************/

#define B_MD_BIT 9
#define B_MD_SEL_PORT    GPIO_PORTB_SEL_V
#define B_MD_DIR_PORT    GPIO_PORTB_DIR_V
#define B_MD_DATA_PORT   GPIO_PORTB_DATA_V


#define MG_DATA1_BIT 10
#define MG_DATA1_SEL_PORT  GPIO_PORTB_SEL_V
#define MG_DATA1_DIR_PORT  GPIO_PORTB_DIR_V
#define MG_DATA1_DATA_PORT GPIO_PORTB_DATA_V

#define MG_DATA2_BIT 11
#define MG_DATA2_SEL_PORT  GPIO_PORTB_SEL_V
#define MG_DATA2_DIR_PORT  GPIO_PORTB_DIR_V
#define MG_DATA2_DATA_PORT GPIO_PORTB_DATA_V

#define MG_DATA3_BIT 12
#define MG_DATA3_SEL_PORT  GPIO_PORTB_SEL_V
#define MG_DATA3_DIR_PORT  GPIO_PORTB_DIR_V
#define MG_DATA3_DATA_PORT GPIO_PORTB_DATA_V


/*************************************************************************************/
/*thermal print*/
/*************************************************************************************/
/*-------------------thermal print (RM)  interrupt define------------------*/

#define RM_INT INTSRC_TIMER2

/*-------------thermal print (RM)  port define and gpio define---------------*/

/*使能步进电机*/
#define PWR_BIT_HAL 0
#define PWR_SEL_PORT  GPIO_PORTC_SEL_V
#define PWR_DIR_PORT  GPIO_PORTC_DIR_V
#define PWR_DATA_PORT GPIO_PORTC_DATA_V  

/*步进电机控制信号*/
#define CMPA_HAL (0x0A)
#define CMPA_SEL_PORT  GPIO_PORTG_SEL_V
#define CMPA_DIR_PORT  GPIO_PORTG_DIR_V
#define CMPA_DATA_PORT GPIO_PORTG_DATA_V 
#define CMPB_HAL 1
#define CMPB_SEL_PORT  GPIO_PORTG_SEL_V
#define CMPB_DIR_PORT  GPIO_PORTG_DIR_V
#define CMPB_DATA_PORT GPIO_PORTG_DATA_V 

/*数据输入使能信号(20)*/
#define DATA_CS_HAL (0x06)
#define DATA_CS_SEL_PORT  GPIO_PORTG_SEL_V
#define DATA_CS_DIR_PORT  GPIO_PORTG_DIR_V
#define DATA_CS_DATA_PORT GPIO_PORTG_DATA_V 

#define CLK_CS_HAL (0x05)
#define CLK_CS_SEL_PORT  GPIO_PORTG_SEL_V
#define CLK_CS_DIR_PORT  GPIO_PORTG_DIR_V
#define CLK_CS_DATA_PORT GPIO_PORTG_DATA_V 

/*控制加热电压源*/
#define TC1_HAL 7
#define TC1_SEL_PORT  GPIO_PORTG_SEL_V
#define TC1_DIR_PORT  GPIO_PORTG_DIR_V
#define TC1_DATA_PORT GPIO_PORTG_DATA_V 

/*缺纸LED信号*/
#define NO_PAGE_LED_HAL 6
#define RM2_SEL_PORT  GPIO_PORTE_SEL_V
#define RM2_DIR_PORT  GPIO_PORTE_DIR_V
#define RM2_DATA_PORT GPIO_PORTE_DATA_V 

/*控制加热块*/
#define PR_DST2_HAL 3
#define RM3_SEL_PORT  GPIO_PORTG_SEL_V
#define RM3_DIR_PORT  GPIO_PORTG_DIR_V
#define RM3_DATA_PORT GPIO_PORTG_DATA_V 
#define PR_DST1_HAL 2
#define RM4_SEL_PORT  GPIO_PORTG_SEL_V
#define RM4_DIR_PORT  GPIO_PORTG_DIR_V
#define RM4_DATA_PORT GPIO_PORTG_DATA_V 

/*数据锁存*/
#define PR_LAT_HAL 1
#define RM5_SEL_PORT  GPIO_PORTC_SEL_V
#define RM5_DIR_PORT  GPIO_PORTC_DIR_V
#define RM5_DATA_PORT GPIO_PORTC_DATA_V 

/*缺纸信号(12,13)*/
#define BL_CHK_IN_HAL 4
#define RM6_SEL_PORT  GPIO_PORTG_SEL_V
#define RM6_DIR_PORT  GPIO_PORTG_DIR_V
#define RM6_DATA_PORT GPIO_PORTG_DATA_V 
#define BLK_SEN_HAL (0x0c)
#define RM7_SEL_PORT  GPIO_PORTG_SEL_V
#define RM7_DIR_PORT  GPIO_PORTG_DIR_V
#define RM7_DATA_PORT GPIO_PORTG_DATA_V 

/*温度控制检测信号*/
#define TE_MPO_HAL 9
#define RM8_SEL_PORT  GPIO_PORTA_SEL_V
#define RM8_DIR_PORT  GPIO_PORTA_DIR_V
#define RM8_DATA_PORT GPIO_PORTA_DATA_V 


#define PR_D0_HAL 0
#define PR_D0_SEL_PORT  GPIO_PORTD_SEL_V
#define PR_D0_DIR_PORT  GPIO_PORTD_DIR_V
#define PR_D0_DATA_PORT GPIO_PORTD_DATA_V 

#define PR_D1_HAL 1
#define PR_D1_SEL_PORT  GPIO_PORTD_SEL_V
#define PR_D1_DIR_PORT  GPIO_PORTD_DIR_V
#define PR_D1_DATA_PORT GPIO_PORTD_DATA_V 

#define PR_D2_HAL 2
#define PR_D2_SEL_PORT  GPIO_PORTD_SEL_V
#define PR_D2_DIR_PORT  GPIO_PORTD_DIR_V
#define PR_D2_DATA_PORT GPIO_PORTD_DATA_V 

#define PR_D3_HAL 3
#define PR_D3_SEL_PORT  GPIO_PORTD_SEL_V
#define PR_D3_DIR_PORT  GPIO_PORTD_DIR_V
#define PR_D3_DATA_PORT GPIO_PORTD_DATA_V 

#define PR_D4_HAL 4
#define PR_D4_SEL_PORT  GPIO_PORTD_SEL_V
#define PR_D4_DIR_PORT  GPIO_PORTD_DIR_V
#define PR_D4_DATA_PORT GPIO_PORTD_DATA_V 

#define PR_D5_HAL 5
#define PR_D5_SEL_PORT  GPIO_PORTD_SEL_V
#define PR_D5_DIR_PORT  GPIO_PORTD_DIR_V
#define PR_D5_DATA_PORT GPIO_PORTD_DATA_V 

#define PR_D6_HAL 6
#define PR_D6_SEL_PORT  GPIO_PORTD_SEL_V
#define PR_D6_DIR_PORT  GPIO_PORTD_DIR_V
#define PR_D6_DATA_PORT GPIO_PORTD_DATA_V 

#define PR_D7_HAL 7
#define PR_D7_SEL_PORT  GPIO_PORTD_SEL_V
#define PR_D7_DIR_PORT  GPIO_PORTD_DIR_V
#define PR_D7_DATA_PORT GPIO_PORTD_DATA_V 

#define RM1_MASK_HAL  (0x00ff)
#define RM1_SEL_PORT  GPIO_PORTD_SEL_V
#define RM1_DIR_PORT  GPIO_PORTD_DIR_V                  
#define RM1_DATA_PORT GPIO_PORTD_DATA_V 

#define RM9_MASK_HAL (0x0010)
#define RM9_SEL_PORT GPIO_PORTF_SEL_V 
#define RM9_DIR_PORT GPIO_PORTF_DIR_V
#define RM9_DATA_PORT GPIO_PORTF_DATA_V 

/* ***************************************************************************/
 /*---FSK interrupt define----------*/
/*****************************************************************************/
#define FSK_INT INTSRC_EXTINT6

#define FSK_INT_TYPE LOW_LEVEL_TRIG  //定义FSK的中断触发类型是低电平触发
 
 /*---------FSK port define and gpio define-----------------------------*/
#define RDATA_BIT 9
#define RDATA_SEL_PORT GPIO_PORTD_SEL_V
#define RDATA_DIR_PORT GPIO_PORTD_DIR_V
#define RDATA_DATA_PORT GPIO_PORTD_DATA_V


#define CLK_BIT 10
#define CLK_SEL_PORT GPIO_PORTD_SEL_V
#define CLK_DIR_PORT GPIO_PORTD_DIR_V
#define CLK_DATA_PORT GPIO_PORTD_DATA_V


#define CDATA_BIT 13
#define CDATA_SEL_PORT GPIO_PORTG_SEL_V
#define CDATA_DIR_PORT GPIO_PORTG_DIR_V
#define CDATA_DATA_PORT GPIO_PORTG_DATA_V


#define CSN_BIT 2
#define CSN_SEL_PORT GPIO_PORTC_SEL_V
#define CSN_DIR_PORT GPIO_PORTC_DIR_V
#define CSN_DATA_PORT GPIO_PORTC_DATA_V

/********************************************************************************/
/*usb host interrupt define*/
/********************************************************************************/
 #define USB_HOST_BIT 6
 #define USB_HOST_INT INTSRC_EXTINT5
 #define USB_HOST_INT_TYPE LOW_LEVEL_TRIG

/********************************************************************************/
/*Magnetic strip card port define*/
/********************************************************************************/
#define CLK_INT_TYPE FALLING_EDGE_TRIG

#define CS_SEL_PORT GPIO_PORTF_SEL_V
#define CS_DIR_PORT GPIO_PORTF_DIR_V
#define CS_DATA_PORT GPIO_PORTF_DATA_V

#define DATA2_SEL_PORT GPIO_PORTF_SEL_V
#define DATA2_DIR_PORT GPIO_PORTF_DIR_V
#define DATA2_DATA_PORT GPIO_PORTF_DATA_V

#define DATA1_SEL_PORT GPIO_PORTE_SEL_V
#define DATA1_DIR_PORT GPIO_PORTE_DIR_V
#define DATA1_DATA_PORT GPIO_PORTE_DATA_V

#define CLK1_INT INTSRC_EXTINT5
#define CLK2_INT INTSRC_EXTINT8

/************************************************************************/
/*gray lcd*/
/************************************************************************/

#define  grylcd_mask1  0x0080
#define  GLCD1_SEL_PORT GPIO_PORTC_SEL_V 
#define  GLCD1_DIR_PORT GPIO_PORTC_DIR_V
#define  GLCD1_DATA_PORT GPIO_PORTC_DATA_V

#define  grylcd_mask2 0x0020
#define  GLCD2_SEL_PORT GPIO_PORTC_SEL_V 
#define  GLCD2_DIR_PORT GPIO_PORTC_DIR_V
#define  GLCD2_DATA_PORT GPIO_PORTC_DATA_V

#define  grylcd_mask3 0x0008
#define  GLCD3_SEL_PORT GPIO_PORTC_SEL_V 
#define  GLCD3_DIR_PORT GPIO_PORTC_DIR_V
#define  GLCD3_DATA_PORT GPIO_PORTC_DATA_V

#define  grylcd_mask4 0x0040
#define  GLCD4_SEL_PORT GPIO_PORTC_SEL_V 
#define  GLCD4_DIR_PORT GPIO_PORTC_DIR_V
#define  GLCD4_DATA_PORT GPIO_PORTC_DATA_V

#define  grylcd_mask5 0x0010
#define  GLCD5_SEL_PORT GPIO_PORTC_SEL_V 
#define  GLCD5_DIR_PORT GPIO_PORTC_DIR_V
#define  GLCD5_DATA_PORT GPIO_PORTC_DATA_V

#define  grylcd_mask6  0x00ff
#define  GLCD6_SEL_PORT GPIO_PORTB_SEL_V 
#define  GLCD6_DIR_PORT GPIO_PORTB_DIR_V
#define  GLCD6_DATA_PORT GPIO_PORTB_DATA_V

#define  grylcd_mask7  0x00f1
#define  GLCD7_SEL_PORT GPIO_PORTC_SEL_V 
#define  GLCD7_DIR_PORT GPIO_PORTC_DIR_V
#define  GLCD7_DATA_PORT GPIO_PORTC_DATA_V
 
#define  grylcd_mask8  0xff00
#define  GLCD8_SEL_PORT GPIO_PORTB_SEL_V 
#define  GLCD8_DIR_PORT GPIO_PORTB_DIR_V
#define  GLCD8_DATA_PORT GPIO_PORTB_DATA_V

#define  grylcd_mask9 0x81
#define  GLCD9_SEL_PORT GPIO_PORTB_SEL_V 
#define  GLCD9_DIR_PORT GPIO_PORTB_DIR_V
#define  GLCD9_DATA_PORT GPIO_PORTB_DATA_V

#define  grylcd_mask10 0x3f
#define  GLCD10_SEL_PORT GPIO_PORTB_SEL_V 
#define  GLCD10_DIR_PORT GPIO_PORTB_DIR_V
#define  GLCD10_DATA_PORT GPIO_PORTB_DATA_V

#define  grylcd_mask11 0x00f8
#define  GLCD11_SEL_PORT GPIO_PORTC_SEL_V 
#define  GLCD11_DIR_PORT GPIO_PORTC_DIR_V
#define  GLCD11_DATA_PORT GPIO_PORTC_DATA_V

#define  grylcd_mask12 0xffff
#define  GLCD12_SEL_PORT GPIO_PORTB_SEL_V 
#define  GLCD12_DIR_PORT GPIO_PORTB_DIR_V
#define  GLCD12_DATA_PORT GPIO_PORTB_DATA_V


/************************************************************************/
/*RCF*/
/************************************************************************/
#define RCF_CONFIG_INT(int_num,int_type)   \
do{    \
	if(int_num == 11){  \
		(*(volatile unsigned long*)GPIO_PORTF_SEL_V) |= 0x0001 ;(*(volatile unsigned long*)GPIO_PORTF_DIR_V) |= 0x0001 ; (*(volatile unsigned long*)GPIO_PORTF_INTRCTL_V) |= int_type;(*(volatile unsigned long*)GPIO_PORTF_INCTL_V) |= 0x0001;(*(volatile unsigned long*)GPIO_PORTF_INTRCLR_V) |= 0x0001;(*(volatile unsigned long*)GPIO_PORTF_INTRCLR_V) = 0x0000;}\
	else if(int_num < 11){      \
		*(volatile unsigned long*)GPIO_PORTA_SEL_V |= 0x1 << (int_num - 1);*(volatile unsigned long*)GPIO_PORTA_DIR_V |= 0x1 << (int_num - 1);*(volatile unsigned long*)GPIO_PORTA_INTRCTL_V |= int_type << ((int_num - 1)*2);*(volatile unsigned long*)GPIO_PORTA_INTRCTL_V |= int_type << (((int_num - 1)*2)+1);*(volatile unsigned long*)GPIO_PORTA_INCTL_V |= 0x1 << (int_num - 1);*(volatile unsigned long*)GPIO_PORTA_INTRCLR_V |= 0x1 << (int_num - 1);*(volatile unsigned long*)GPIO_PORTA_INTRCLR_V = 0x0000;}\
}while(0)

/*#define RCF_INT INTSRC_EXINT9
#define RCF_MF_RST_BIT 5
#define RCF_MF_CS_BIT 7

#define RCF_MF_SEL_PORT GPIO_PORTB_SEL_V
#define RCF_MF_DIR_PORT GPIO_PORTB_DIR_V
#define RCF_MF_DATA_PORT GPIO_PORTD_DATA_V

#define RCF_NUM1 10
#define RCF_NUM2 9
#define RCF_NUM3 8
#define RCF_NUM4 6
#define RCF_NUM5 5
#define RCF_NUM6 7

#define RCF_SSI_SEL_PORT GPIO_PORTD_SEL_V
#define RCF_SSI_DIR_PORT GPIO_PORTD_DIR_V
#define RCF_SSI_DATA_PORT GPIO_PORTD_DATA_V*/
#define RCF_INT INTSRC_EXTINT6
#define RCF_MF_RST_BIT 6
#define RCF_MF_CS_BIT  7

#define RCF_MF_SEL_PORT GPIO_PORTG_SEL_V
#define RCF_MF_DIR_PORT GPIO_PORTG_DIR_V
#define RCF_MF_DATA_PORT GPIO_PORTG_DATA_V

#define RCF_NUM1 0
#define RCF_NUM2 1
#define RCF_NUM3 2
#define RCF_NUM4 4
#define RCF_NUM5 3

#define RCF_SSI_SEL_PORT GPIO_PORTD_SEL_V
#define RCF_SSI_DIR_PORT GPIO_PORTD_DIR_V
#define RCF_SSI_DATA_PORT GPIO_PORTD_DATA_V
 
 /* --------------------------------------------------------------------
 * Dot-Matrix Printer Interface
 针式打印原理图示意图，具体请参考各类板的原理图
                                     
				 PWR_CTL(PG5)
          		  |	LF_M_CON(G12)  
LF_M_PA(PG10)----/-------/---------------|-------------------------  -|
					      |		            |					      |--------------------------        |
                                              |                	    |										                                            |==>控制进纸电机走纸
LF_M_PB(PG6) ----/-------/---------------|-------------------------   |
				              |		 						      |--------------------------     -|
					      |
          		  |	LF_M_CON(G12)  
CR_M_PB(PG3) ----/-------/---------------|-------------------------	-|
					      |			    |												                            |==>控制托架电机
CR_M_PA(PC1) ----/-------/---------------|-------------------------  -|
					      |		
					  |	HEAD_TRIG(PG7)
PD0	-----------/-----/--------------->-------------------------HEAD#1	-|
PD1	-----------/-----/--------------->-------------------------HEAD#2	 |
PD2	-----------/-----/--------------->-------------------------HEAD#3	 |
PD3	-----------/-----/--------------->-------------------------HEAD#4	 |
PD4	-----------/-----/--------------->-------------------------HEAD#5	 |==>控制打印头出针
PD5	-----------/-----/--------------->-------------------------HEAD#6	 |
PD6	-----------/-----/--------------->-------------------------HEAD#7	 |
PD7	-----------/-----/--------------->-------------------------HEAD#8	-|

 * ------------------------------------------------------------------------ */

/*-------------------Dot-Matrix Printer Interrupt define------------------*/
/******<NOTHING>******/

/*---------Dot-Matrix Printer port define and gpio define-----------------*/
#define DATA_MASK (0x0ff)
#define DATA_SEL_PORT		GPIO_PORTD_SEL_V
#define DATA_DIR_PORT		GPIO_PORTD_DIR_V
#define DATA_DATA_PORT		GPIO_PORTD_DATA_V 

#define LF_M_PA 10
#define LF_M_PA_SEL_PORT	GPIO_PORTG_SEL_V
#define LF_M_PA_DIR_PORT	GPIO_PORTG_DIR_V
#define LF_M_PA_DATA_PORT	GPIO_PORTG_DATA_V 

#define LF_M_PB 6
#define LF_M_PB_SEL_PORT	GPIO_PORTG_SEL_V
#define LF_M_PB_DIR_PORT	GPIO_PORTG_DIR_V
#define LF_M_PB_DATA_PORT	GPIO_PORTG_DATA_V 

#define LF_M_CON 12
#define LF_M_CON_SEL_PORT	GPIO_PORTG_SEL_V
#define LF_M_CON_DIR_PORT	GPIO_PORTG_DIR_V
#define LF_M_CON_DATA_PORT	GPIO_PORTG_DATA_V 

#define CR_M_PA 1
#define CR_M_PA_SEL_PORT	GPIO_PORTC_SEL_V
#define CR_M_PA_DIR_PORT	GPIO_PORTC_DIR_V
#define CR_M_PA_DATA_PORT	GPIO_PORTC_DATA_V 

#define CR_M_PB 3
#define CR_M_PB_SEL_PORT	GPIO_PORTG_SEL_V
#define CR_M_PB_DIR_PORT	GPIO_PORTG_DIR_V
#define CR_M_PB_DATA_PORT	GPIO_PORTG_DATA_V 

#define PWR_CTL 5
#define PWR_CTL_SEL_PORT	GPIO_PORTG_SEL_V
#define PWR_CTL_DIR_PORT	GPIO_PORTG_DIR_V
#define PWR_CTL_DATA_PORT	GPIO_PORTG_DATA_V 

#define HP_SEN	4
#define HP_SEN_SEL_PORT		GPIO_PORTG_SEL_V
#define HP_SEN_DIR_PORT		GPIO_PORTG_DIR_V
#define HP_SEN_DATA_PORT	GPIO_PORTG_DATA_V 

#define PE_SEN	9
#define PE_SEN_SEL_PORT		GPIO_PORTA_SEL_V
#define PE_SEN_DIR_PORT		GPIO_PORTA_DIR_V
#define PE_SEN_DATA_PORT	GPIO_PORTA_DATA_V 

#define BM_SEN	0
#define BM_SEN_SEL_PORT		GPIO_PORTC_SEL_V
#define BM_SEN_DIR_PORT		GPIO_PORTC_DIR_V
#define BM_SEN_DATA_PORT	GPIO_PORTC_DATA_V 

#define PRT_HEAD_TRIG 7
#define PRT_HEAD_TRIG_SEL_PORT		GPIO_PORTG_SEL_V
#define PRT_HEAD_TRIG_DIR_PORT		GPIO_PORTG_DIR_V
#define PRT_HEAD_TRIG_DATA_PORT		GPIO_PORTG_DATA_V 

#define CR_IOB 2
#define CR_IOB_SEL_PORT		GPIO_PORTG_SEL_V
#define CR_IOB_DIR_PORT		GPIO_PORTG_DIR_V
#define CR_IOB_DATA_PORT	GPIO_PORTG_DATA_V

#define CR_IOA 1
#define CR_IOA_SEL_PORT		GPIO_PORTG_SEL_V
#define CR_IOA_DIR_PORT		GPIO_PORTG_DIR_V
#define CR_IOA_DATA_PORT	GPIO_PORTG_DATA_V
 
#define I2C_SDA_DIR    GPIO_PORTC_DIR_V 
#define I2C_SDA_SEL    GPIO_PORTC_SEL_V
#define I2C_SDA_DATA   GPIO_PORTC_DATA_V

#define I2C_SCL_DIR    GPIO_PORTD_DIR_V 
#define I2C_SCL_SEL    GPIO_PORTD_SEL_V
#define I2C_SCL_DATA   GPIO_PORTD_DATA_V
#endif
