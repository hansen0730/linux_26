/*
 * include/asm-arm/arch-sep4020/platform.h
 *
 * Various bits of code used by platform-level code.
 *
 * Author: Zhang Min <zmdesperado@gmail.com>
 *
 * Copyright 2010 (c) Southeast University 
 * 
 * This file is licensed under  the terms of the GNU General Public 
 * License version 2. This program is licensed "as is" without any 
 * warranty of any kind, whether express or implied.
 */

#ifndef __ASSEMBLY__

#define I2C_SDA 0x08
#define I2C_SCL 0x04

struct sep4020_i2c_pins {
	unsigned long sda_pin;
	unsigned long scl_pin;
};


#endif /*  !__ASSEMBLY__ */
